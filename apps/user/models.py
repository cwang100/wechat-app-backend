from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Person(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE)

    nickname = models.CharField('微信昵称', max_length=100)
    openid = models.CharField('用户标识', max_length=100, default='default')
    cookie = models.CharField('用户认证标识', max_length=100, default='')
    avatarUrl = models.URLField('头像链接', default='', null=False)
    GENDER = (
        (1, '男'),
        (2, '女'),
        (0, '未知')
    )

    gender = models.CharField('性别', max_length=10, choices=GENDER)
    city = models.CharField('城市', max_length=100)
    province = models.CharField('省份', max_length=100)
    bonuspoint = models.IntegerField('点数', default=0)

    def __str__(self):
        return self.nickname

class Question(models.Model):
    owner = models.ForeignKey('Person', verbose_name='提问者', blank=True, null=True, on_delete=models.SET_NULL)
    description = models.CharField('问题描述', max_length=700)
    price = models.IntegerField('赏金额度', default=0)
    PRICE_TYPE = (
        (1, 'POINT'),
        (0, 'YUAN')
    )
    price_type = models.CharField('赏金类型', max_length=10, choices=PRICE_TYPE,default='YUAN')
    STATUS = (
        (2, 'CLOSED'),
        (1, 'ACTIVE'),
        (0, 'DRAFT')
    )

    status = models.CharField('问题状态', max_length=10, choices=STATUS,default='ACTIVE')

    created_time = models.DateTimeField('创建时间', auto_now_add=True)
    last_active_time = models.DateTimeField('最近操作时间', auto_now=True)

    def __str__(self):

        return str(self.id)

class Answer(models.Model):
    owner = models.ForeignKey('Person', verbose_name='回答者', blank=True, null=True, on_delete=models.SET_NULL)
    target = models.ForeignKey('Question', verbose_name='问题', on_delete=models.CASCADE)
    description = models.CharField('回答内容', max_length=500)
    STATUS = (
        (3, 'DECLINED'),
        (2, 'ACCEPTED'),
        (1, 'WAITING'),
        (0, 'DRAFT')
    )

    status = models.CharField('回答状态', max_length=10, choices=STATUS,default='WAITING')
    contact_info = models.CharField('联系方式', max_length=30, blank=True, null=True)
    forward_from = models.ForeignKey('Forward', verbose_name='转发自', blank=True, null=True, on_delete=models.SET_NULL)
    created_time = models.DateTimeField('创建时间', auto_now_add=True)
    last_active_time = models.DateTimeField('最近操作时间', auto_now=True)

    def __str__(self):

        return str(self.created_time)

class Forward(models.Model):
    question = models.ForeignKey('Question', verbose_name='问题', on_delete=models.CASCADE)
    forwarder = models.ForeignKey('Person', verbose_name='转发者', blank=True, null=True, on_delete=models.SET_NULL)
    forward_from = models.ForeignKey('Forward', verbose_name='转发自', related_name='forward_id', blank=True, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return str(self.id)