from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^login', views.verify_user),
    url(r'^get_single_question', views.get_single_question),
    url(r'^get_random_questions', views.get_random_questions),
    url(r'^get_single_answer', views.get_single_answer),
    url(r'^get_questions', views.get_questions),
    url(r'^get_answers', views.get_answers),
    url(r'^get_question_without_answer', views.get_question_without_answer),
    url(r'^create_question', views.create_question),
    url(r'^create_answer', views.create_answer),
    url(r'^modify_answer', views.modify_answer),
    url(r'^create_forward', views.create_forward),
]
