from django.shortcuts import render
from django.http import JsonResponse
from .models import Person, Question, Answer, Forward
from django.core import serializers
from django.contrib.auth import logout, login, authenticate
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from django.forms.models import model_to_dict

from .checkuser import checkdata

import requests
import random
# Create your views here.

@csrf_exempt
def verify_user(request):
    try:
        if request.method == 'POST':
            # print(request.POST)
            # 初始化返回的字典
            data = {}

            # 获取小程序数据
            code = request.POST.get('code')
            encrypteddata = request.POST.get('encryptedData')
            iv = request.POST.get('iv')

            # 检查用户
            res = checkdata(code, encrypteddata, iv)
            # 检查不通过
            errorinfo = res.get('error', None)
            if errorinfo:
                return JsonResponse(res)

            openid = res['openId']

            user = authenticate(username=openid, password=openid)
            # 登陆用户并保存 cookie
            if user:
                login(request, user)
                query_user = Person.objects.get(openid=openid)
                query_user.cookie = res['cookie']
                query_user.save()

                data['status'] = '已登录'
            # 新建用户
            else:
                user_ins = User.objects.create_user(
                    username=openid,
                    password=openid
                )
                profile = Person.objects.create(
                    user=user_ins,
                    openid=openid,
                    cookie=res['cookie'],
                    nickname=res['nickName'],
                    city=res['city'],
                    avatarUrl=res['avatarUrl'],
                    province=res['province'],
                    gender=res['gender']

                )

                new_user = authenticate(username=openid, password=openid)
                login(request, new_user)

                data['status'] = '已创建并登录'

            data['info'] = res
            # print('最终返回信息',data)

            return JsonResponse(data)

        data = {'error': '仅接受POST请求'}
        return JsonResponse(data)
    except:
        data = {'error': '服务器错误'}
        return JsonResponse(data)

@csrf_exempt
def get_single_question(request):
    try:
        # pass
        if request.method == 'POST':

            data = {}
            question_id = request.POST.get('question_id')
            cookie = request.POST.get('cookie')
            
            persons = Person.objects.filter(cookie=cookie)

            if len(persons) != 1:  
                data = {'error': '用户错误'}
                return JsonResponse(data)

            person = persons[0]

            questions = Question.objects.filter(id=question_id)
            if len(questions) != 1:
                data = {'error': '无此问题'}
                return JsonResponse(data)
            question = questions[0]
            owner = question.owner
            question_detail = model_to_dict(question)
            question_detail['avatarUrl'] = owner.avatarUrl
            question_detail['owner_name'] = owner.nickname
            question_detail['create_time'] = question.created_time.strftime('%Y-%m-%d')
            answers = Answer.objects.filter(target=question)
            answer_details = []
            for answer in answers:
                detailed_answer = model_to_dict(answer)
                answer_person = answer.owner
                detailed_answer['avatarUrl'] = answer_person.avatarUrl
                detailed_answer['owner_name'] = answer_person.nickname
                detailed_answer['create_time'] = answer.created_time.strftime('%Y-%m-%d')
                answer_details.append(detailed_answer)

            data['question'] = question_detail
            data['answers'] = answer_details

            return JsonResponse(data)


        data = {'error': '仅接受POST请求'}
        return JsonResponse(data)
    except:
        data = {'error': '服务器错误'}
        return JsonResponse(data)

@csrf_exempt
def get_single_answer(request):
    try:
        # pass
        if request.method == 'POST':

            data = {}
            answer_id = request.POST.get('answer_id')
            cookie = request.POST.get('cookie')
            
            persons = Person.objects.filter(cookie=cookie)

            if len(persons) != 1:  
                data = {'error': '用户错误'}
                return JsonResponse(data)

            person = persons[0]

            answers = Answer.objects.filter(id=answer_id)
            if len(answers) != 1:
                data = {'error': '无此答案'}
                return JsonResponse(data)
            answer = answers[0]
            owner = answer.owner
            answer_detail = model_to_dict(answer)
            answer_detail['avatarUrl'] = owner.avatarUrl
            answer_detail['owner_name'] = owner.nickname
            answer_detail['create_time'] = answer.created_time.strftime('%Y-%m-%d')

            question = answer.target
            q_owner = question.owner
            question_detail = model_to_dict(question)
            question_detail['avatarUrl'] = q_owner.avatarUrl
            question_detail['owner_name'] = q_owner.nickname

            data['answer'] = answer_detail
            data['question'] = question_detail

            return JsonResponse(data)


        data = {'error': '仅接受POST请求'}
        return JsonResponse(data)
    except:
        data = {'error': '服务器错误'}
        return JsonResponse(data)

@csrf_exempt
def get_question_without_answer(request):
    try:
        # pass
        if request.method == 'POST':

            data = {}
            question_id = request.POST.get('question_id')
            cookie = request.POST.get('cookie')
            
            persons = Person.objects.filter(cookie=cookie)

            if len(persons) != 1:  
                data = {'error': '用户错误'}
                return JsonResponse(data)

            person = persons[0]

            questions = Question.objects.filter(id=question_id)
            if len(questions) != 1:
                data = {'error': '无此问题'}
                return JsonResponse(data)
            question = questions[0]
            owner = question.owner
            question_detail = model_to_dict(question)
            question_detail['avatarUrl'] = owner.avatarUrl
            question_detail['owner_name'] = owner.nickname
            question_detail['create_time'] = question.created_time.strftime('%Y-%m-%d')

            data['question'] = question_detail

            return JsonResponse(data)


        data = {'error': '仅接受POST请求'}
        return JsonResponse(data)
    except:
        data = {'error': '服务器错误'}
        return JsonResponse(data)

@csrf_exempt
def get_random_questions(request):
    try:
    # pass
        if request.method == 'POST':

            data = {}
            cookie = request.POST.get('cookie')
            
            persons = Person.objects.filter(cookie=cookie)

            if len(persons) != 1:
                data = {'error': '用户错误'}
                return JsonResponse(data)

            person = persons[0]

            count = Question.objects.all().count()
            qnum = min(count, 10)
            slice = random.random() * (count - qnum)
            
            questions = Question.objects.all()[slice: slice+qnum]
            question_details = []
            for question in questions:
                detailed_question = model_to_dict(question)
                owner = question.owner
                detailed_question['avatarUrl'] = owner.avatarUrl
                detailed_question['owner_name'] = owner.nickname
                detailed_question['create_time'] = question.created_time.strftime('%Y-%m-%d')
                question_details.append(detailed_question)

            data['questions'] = question_details

            return JsonResponse(data)


        data = {'error': '仅接受POST请求'}
        return JsonResponse(data)
    except:
        data = {'error': '服务器错误'}
        return JsonResponse(data)


@csrf_exempt
def get_questions(request):
    try:
        # pass
        if request.method == 'POST':

            data = {}
            cookie = request.POST.get('cookie')
            
            persons = Person.objects.filter(cookie=cookie)

            if len(persons) != 1:
                data = {'error': '用户错误'}
                return JsonResponse(data)

            person = persons[0]

            questions = Question.objects.filter(owner=person)
            question_details = []
            for question in questions:
                detailed_question = model_to_dict(question)
                detailed_question['avatarUrl'] = person.avatarUrl
                detailed_question['owner_name'] = person.nickname
                detailed_question['create_time'] = question.created_time.strftime('%Y-%m-%d')
                question_details.append(detailed_question)

            data['questions'] = question_details

            return JsonResponse(data)


        data = {'error': '仅接受POST请求'}
        return JsonResponse(data)
    except:
        data = {'error': '服务器错误'}
        return JsonResponse(data)

@csrf_exempt
def get_answers(request):
    try:
        # pass
        if request.method == 'POST':

            data = {}
            cookie = request.POST.get('cookie')
            
            persons = Person.objects.filter(cookie=cookie)

            if len(persons) != 1:
                
                data = {'error': '用户错误'}
                return JsonResponse(data)

            person = persons[0]

            answers = Answer.objects.filter(owner=person)
            answer_details = []
            for answer in answers:
                detailed_answer = model_to_dict(answer)
                answer_person = answer.owner
                detailed_answer['avatarUrl'] = answer_person.avatarUrl
                detailed_answer['owner_name'] = answer_person.nickname
                detailed_answer['question_des'] = answer.target.description
                detailed_answer['question_owner'] = answer.target.owner.nickname
                detailed_answer['create_time'] = answer.created_time.strftime('%Y-%m-%d')
                answer_details.append(detailed_answer)

            data['answers'] = answer_details

            return JsonResponse(data)


        data = {'error': '仅接受POST请求'}
        return JsonResponse(data)
    except:
        data = {'error': '服务器错误'}
        return JsonResponse(data)

@csrf_exempt
def create_question(request):
    try:
        if request.method == 'POST':
            data = {}
            cookie = request.POST.get('cookie')
            description = request.POST.get('description')
            price = request.POST.get('price')
            price_type = request.POST.get('pricetype')

            persons = Person.objects.filter(cookie=cookie)
            if len(persons) != 1:
                data = {'error': '用户错误'}
                return JsonResponse(data)
            person = persons[0]

            # 检查内容是否为空
            if description == '':
                data = {'error': '内容不能为空'}
                return JsonResponse(data)

            if price_type == '':
                data = {'error': '金额类型不能为空'}
                return JsonResponse(data)

            new_question = Question.objects.create(
                description=description,
                price=price,
                price_type=price_type,
                owner=person,
            )

            data['status'] = '添加成功'
            data['result'] = model_to_dict(new_question)
            return JsonResponse(data)

        data = {'error': '仅接受POST请求'}
        return JsonResponse(data)
    except:
        data = {'error': '服务器错误'}
        return JsonResponse(data)

@csrf_exempt
def create_answer(request):
    try:
        if request.method == 'POST':
            data = {}
            cookie = request.POST.get('cookie')
            description = request.POST.get('description')
            contact_info = request.POST.get('contact_info')
            target_id = request.POST.get('target_id')
            forward_id = request.POST.get('forward_id')

            persons = Person.objects.filter(cookie=cookie)
            if len(persons) != 1:
                data = {'error': '用户错误'}
                return JsonResponse(data)
            person = persons[0]

            if forward_id == '':
                forward_target = None
            else:
                forward_targets = Forward.objects.get(id=forward_id)

            target_questions = Question.objects.filter(id=target_id)
            if len(target_questions) != 1:
                data = {'error': '问题错误'}
                return JsonResponse(data)

            target_question = target_questions[0]
            #if person == target_question.owner:
                #data = {'error': '不能回答自己的问题'}
                #return JsonResponse(data)
            if forward_target != None:
                new_forward = Forward.objects.create(
                    question=question,
                    forward_from=forward_target,
                    forwarder=person,
                )
            else:
                new_forward = None
            # 检查内容是否为空
            if description == '':
                data = {'error': '内容不能为空'}
                return JsonResponse(data)

            new_answer = Answer.objects.create(
                description=description,
                target=target_question,
                owner=person,
                contact_info = contact_info,
                forward_from = new_forward
            )

            data['status'] = '添加成功'
            data['result'] = model_to_dict(new_answer)
            return JsonResponse(data)

        data = {'error': '仅接受POST请求'}
        return JsonResponse(data)
    except:
        data = {'error': '服务器错误'}
        return JsonResponse(data)

@csrf_exempt
def create_forward(request):
    #try:
    if request.method == 'POST':
        data = {}
        cookie = request.POST.get('cookie')
        question_id = request.POST.get('question_id')
        forward_from = request.POST.get('forward_id')

        persons = Person.objects.filter(cookie=cookie)
        if len(persons) != 1:
            data = {'error': '用户错误'}
            return JsonResponse(data)
        person = persons[0]

        questions = Question.objects.filter(id=question_id)
        if len(questions) != 1:
            data = {'error': '问题不能为空'}
            return JsonResponse(data)

        question = questions[0]
        owner = question.owner
        question_detail = model_to_dict(question)
        question_detail['owner_name'] = owner.nickname

        try:
            forward_id = int(forward_from)
            forward_targets = Forward.objects.filter(id=forward_from)
            if len(forward_targets) != 1:
                forward_target = None
            else:
                forward_target = forward_targets[0]
        except ValueError:
            forward_target = None
        if person == question.owner and forward_target != None:
            data = {'error': '不能转发自己的问题'}
            return JsonResponse(data)

        new_forward, created= Forward.objects.get_or_create(
            question=question,
            forward_from=forward_target,
            forwarder=person,
        )

        if created:
            data['status'] = '转发成功'
        else:
            data['status'] = '转发过了'
        data['result'] = model_to_dict(new_forward)
        data['question'] = question_detail
        return JsonResponse(data)

    data = {'error': '仅接受POST请求'}
    return JsonResponse(data)
    #except:
        #data = {'error': '服务器错误'}
        #return JsonResponse(data)

@csrf_exempt
def modify_answer(request):
    try:
        if request.method == 'POST':
            data = {}
            cookie = request.POST.get('cookie')
            description = request.POST.get('description')
            contact_info = request.POST.get('contact_info')
            answer_id = request.POST.get('answer_id')
            status = request.POST.get('answer_status')
            persons = Person.objects.filter(cookie=cookie)
            if len(persons) != 1:
                data = {'error': '用户错误'}
                return JsonResponse(data)
            person = persons[0]

            target_answers = Answer.objects.filter(id=answer_id)
            if len(target_answers) != 1:
                data = {'error': '问题错误'}
                return JsonResponse(data)   
            else:
                target_answer = target_answers[0]

            if description != '' and description != None:
                if person != target_answer.owner:
                    data = {'error': '不能修改此问题'}
                    return JsonResponse(data)
                else:
                    target_answer.description = description
            if contact_info != '' and contact_info != None:
                if person != target_answer.owner:
                    data = {'error': '不能修改此问题'}
                    return JsonResponse(data)
                else:
                    target_answer.contact_info = contact_info
            if status != '' and status != None:
                if person != target_answer.target.owner:
                    data = {'error': '不能修改此问题'}
                    return JsonResponse(data)
                else:
                    target_answer.status = status
            target_answer.save()

            data['status'] = '添加成功'
            data['result'] = model_to_dict(target_answer)
            return JsonResponse(data)

        data = {'error': '仅接受POST请求'}
        return JsonResponse(data)
    except:
        data = {'error': '服务器错误'}
        return JsonResponse(data)


